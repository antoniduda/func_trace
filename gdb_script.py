import gdb


class QuietBreakpoint(gdb.Breakpoint):
    def stop(self):
        gdb.execute(self.commands)
        return False


class QuietFinishBreakpoint(gdb.FinishBreakpoint):
    def stop(self):
        gdb.execute(self.commands)
        return False


class FuncTrace(gdb.Command):
    def __init__(self):
        self.func_name = ""
        self.continue_on_break = True
        self.break_on = []
        self.saved_frames = []
        self.b = None
        self.ommit_recursive = gdb.Parameter(
            "FuncTraceOmmitRecursive", gdb.COMMAND_DATA, gdb.PARAM_BOOLEAN
        )
        self.hit_fin = True
        super(FuncTrace, self).__init__(
            "functrace", gdb.COMMAND_USER, prefix=True
        )

    def break_fin_handler(self, idx):
        self.saved_frames[idx][1] = self._backup_frame(gdb.newest_frame())

    def _backup_stack(self, frame):
        stack = []
        frame.select()
        for i in range(0, 40, 8):
            stack_addr = gdb.parse_and_eval(f"$sp + {i}")
            stack_value = stack_addr.cast(
                gdb.lookup_type("long long").pointer()
            ).referenced_value()
            try:
                stack_value_points_to = (
                    stack_value.cast(gdb.lookup_type("long long").pointer())
                    .referenced_value()
                    .format_string(format="z")
                )
            except gdb.MemoryError:
                stack_value_points_to = None
            stack.append(
                (
                    stack_addr.format_string(format="z"),
                    stack_value.format_string(format="z"),
                    stack_value_points_to,
                )
            )
        return stack

    def _backup_frame(self, frame):
        frame_dict = {}
        for reg in frame.architecture().registers():
            register_val = frame.read_register(reg)
            try:
                referenced_val = str(
                    register_val.cast(
                        gdb.lookup_type("long long").pointer()
                    ).referenced_value()
                )
                frame_dict[reg.name] = (str(register_val), referenced_val)
            except:
                frame_dict[reg.name] = (str(register_val), None)
        frame_dict["stack"] = self._backup_stack(frame)
        return frame_dict

    def break_handler(self):
        if self.ommit_recursive.value and not self.hit_fin:
            return
        self.hit_fin = False

        frame = self._backup_frame(gdb.newest_frame())
        self.saved_frames.append([frame, None])

        b = QuietFinishBreakpoint(internal=1)
        b.commands = (
            f"python ftrace.break_fin_handler({len(self.saved_frames)-1})\n"
        )

    def invoke(self, argument, from_tty):
        if not argument:
            print("Usage: functrace [function name]")
            return
        if self.b:
            print(f"Already tracing {self.func_name}.\nIf you want to trace another function first run functrace finish.")
        self.func_name = argument
        self.b = QuietBreakpoint(self.func_name, internal=1)
        self.b.commands = "python ftrace.break_handler()\n"


class FuncTraceShow(gdb.Command):
    def __init__(self):
        self.argument_registers = ["rdi", "rsi", "rdx"]
        self.default_fin_output = ["rax"]
        super(FuncTraceShow, self).__init__(
            "functrace show", gdb.COMMAND_USER, prefix=True
        )

    def _print_register(self, reg_name, call_num, ret=False):
        try:
            if not ret:
                self._print_value(
                    ftrace.saved_frames[call_num][0][reg_name], end="\n"
                )
            else:
                self._print_value(
                    ftrace.saved_frames[call_num][1][reg_name], end="\n"
                )
        except KeyError:
            print(f"There is no register saved with name {reg_name}.")
        except IndexError:
            print(f"No function was saved with index {call_num}.")

    def _print_value(self, d, end=""):
        if d[1]:
            print(f"{d[0]} -> {d[1]}", end=end)
        else:
            print(f"{d[0]}", end=end)

    def _show(self):
        for i, frame_list in enumerate(ftrace.saved_frames):
            print(f"{i}: {ftrace.func_name}(", end="")
            for reg in self.argument_registers:
                self._print_value(frame_list[0][reg], end=", ")
            print("\b\b) returned ", end="")
            for reg in self.default_fin_output:
                self._print_value(frame_list[1][reg])
            print("\b\b")

    def invoke(self, args, from_tty):
        if not args:
            self._show()
        else:
            ret = False
            args_list = args.split(" ")
            if args_list[0] == "return":
                ret = True
                args_list = args_list[1:]
            try:
                call_num = int(args_list[0])
            except ValueError:
                return
            reg_name = args_list[1]
            self._print_register(reg_name, call_num, ret)


class FuncTraceShowStack(gdb.Command):
    def __init__(self):
        super(FuncTraceShowStack, self).__init__(
            "functrace show stack", gdb.COMMAND_USER
        )

    def _print_stack(self, stack):
        for stack_value in stack:
            if stack_value[2]:
                print(
                    f"{stack_value[0]}| {stack_value[1]} -> {stack_value[2]}"
                )
            else:
                print(f"{stack_value[0]}| {stack_value[1]}")

    def invoke(self, args, from_tty):
        if not args:
            return

        ret = False
        if args.startswith("return"):
            args = args[7:]
            ret = True
        try:
            call_num = int(args)
        except ValueError:
            return
        if not ret:
            self._print_stack(ftrace.saved_frames[call_num][0]["stack"])
        else:
            self._print_stack(ftrace.saved_frames[call_num][1]["stack"])


class FuncTraceClear(gdb.Command):
    def __init__(self):
        super(FuncTraceClear, self).__init__(
            "functrace clear", gdb.COMMAND_USER
        )

    def invoke(self, args, from_tty):
        ftrace.saved_frames = []

class FuncTraceFinish(gdb.Command):
    def __init__(self):
        super(FuncTraceFinish, self).__init__(
            "functrace finish", gdb.COMMAND_USER
        )

    def invoke(self, args, from_tty):
        ftrace.saved_frames = []
        ftrace.b.delete()
        ftrace.b = None


ftrace = FuncTrace()
ftrace_show = FuncTraceShow()
ftrace_clear = FuncTraceClear()
ftrace_show_stack = FuncTraceShowStack()
ftrace_finish = FuncTraceFinish()
