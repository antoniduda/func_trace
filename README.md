# functrace

Simple script for gdb allowing to print out arguments and return value of selected function.

To use this script you need to run `source ./gdb_script.py` inside gdb. You can
also add this to your's ~/.gdbinit if you want gdb to load this script every
time.

# Basic usage
`functrace [function name/address]` - starts tracing specified function.  
`functrace show` - displays calls to traced function with arguments and return value.  
```
gef➤  functrace show
0: malloc(0x400, 0x7ffff7f46073 -> 0x6225206125000200, 0x88) returned 0x4052a0 -> 0x0
1: malloc(0x400, 0x7ffff7f46073 -> 0x6225206125000200, 0x88) returned 0x4056b0 -> 0x0
```
`functrace show [num] [register]` - displays value of specified register at entry of function with index = num. Index is the number displayed in front of a call when running functrace show.  
```
gef➤  functrace show
0: malloc(0x400, 0x7ffff7f46073 -> 0x6225206125000200, 0x88) returned 0x4052a0 -> 0x0
1: malloc(0x400, 0x7ffff7f46073 -> 0x6225206125000200, 0x88) returned 0x4056b0 -> 0x0
gef➤  functrace show 0 rdi
0x400
gef➤  functrace show 0 rax
0x3ff
```
`functrace show return [num] [register]` - same as above except shows register value when the function returned.  
`functrace show stack [num]` - shows stack of function on entry.  
`functrace show stack return [num]` - shows stack of fuunction after return.  
`functrace clear` - clears the information that is already stored.  
`functrace finish` - stops tracing function.  

Only one function at a time can be traced.

# Supported architectures
By default x86-64 architecture is supported, however with not that much effort
the script could be adapted to other architectures. When saving frame values of
all registers are saved, so if the arguments aren't properly displayed when
using `functrace show` you can try doing the same manually - `functrace show
[num] [register]`.
